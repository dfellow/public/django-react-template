# django-react-template

<h2> Require </h2>

```
    # backend
    docker >= 19.03.12
    docker-compose >= 1.27.2
    python >= 3.7
    virtualenv >= 16.7

    # frontend
    node >= 12.8.0
    yarn >= 1.17.3
```
<br>

<h2>Django
<h3> Create Django project </h3>

```
    sh start-django.sh
    # or
    ./start-django.sh
```

<p> edit database in settings.py </p>

```

import os
...

SECRET_KEY = os.environ.get('DJANGO_SECRET')
DEBUG = os.environ.get('STATE', None) == "dev"
if DEBUG:print("STATE :", os.environ.get('STATE'))

INSTALLED_APPS = [
    ...
    'corsheaders',
    'rest_framework',
    ...
]

MIDDLEWARE = [
    ...
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('POSTGRES_DB', None),
        'USER': os.environ.get('POSTGRES_USER', None),
        'PASSWORD': os.environ.get('POSTGRES_PASSWORD', None),
        'HOST': os.environ.get('DB_HOST', None),
        'PORT': os.environ.get('DB_PORT', None),
    }
}

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_WHITELIST = [
    'http://localhost:30000',
]
CORS_ORIGIN_REGEX_WHITELIST = [
    'http://localhost:3000',
]
```

<h3> Architech Backend </h3>

```
.
├── Dockerfile
├── api
│   ├── urls.py
│   └── v1
│   |   ├── routers.py
│   |   ├── serializers.py
│   |   └── viewsets.py
|   |____
|    ... v2
├── backend
│   ├── __init__.py
│   ├── asgi.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
├── manage.py
├── requirements.txt
└── runserver.sh
```

# Env
```
PROJECT_NAME=<projectname>
STATE=<dev/production>

DJANGO_SECRET=<DJANGO SECRETKEY>
DJANGO_ALLOW_ASYNC_UNSAFE=true
PYTHONUNBUFFERED=1

POSTGRES_DB=<DB NAME>
POSTGRES_USER=<DB USER>
POSTGRES_PASSWORD=<DB PASSWORD>
DB_HOST=db
DB_PORT=5432

NODE_ENV=development
CI=true
```